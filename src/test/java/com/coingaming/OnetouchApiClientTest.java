package com.coingaming;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.coingaming.rest.model.ResponseUrl;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class OnetouchApiClientTest {

  @Inject
  private OnetouchApiClient onetouchApiClient;

  @Test
  void makeCall() throws Exception {

    final ResponseUrl response = onetouchApiClient.makeCall("operator/generic/v2/game/url");

    //Response(url=https://test-core.onetouch.io/cmt/v1/game/juicy7/latest/index.html?
    // config_id=6049e80eb75c015ee6341c6c&
    // session_id=9efdc0f4-0a2c-46c6-990f-4eff7ffe387c&
    // lobby_url=https%3A%2F%2Fexamplecasino.io&
    // deposit_url=https%3A%2F%2Fonetouch.io&device_platform=desktop&engine_base_url=https://test-core.onetouch.io&
    // sub_partner_id=otcasino&
    // lang=en&
    // ts=1627544847197)
    assertTrue(response.getUrl().startsWith("https://test-core.onetouch.io/cmt/v1/game/juicy7/latest/index.html?"), response.toString());

  }
}
