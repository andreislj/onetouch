package com.coingaming.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class SignatureValidationServiceTest {

  @InjectMocks
  private SignatureValidationService signatureValidationService;

  @Spy
  private PublicKeyProvider publicKeyProvider;

  @Test
  void validateSuccess() {

    final boolean isSignatureValid = signatureValidationService.validate("123456",
        "YsUAI+s8wcGwB3ZsJFfRpOu6tIzZweZXC4qUhj9f7qFcuuQuj8cE9ETM5D1MdEoSIAdGDuGY2QbhxVxtaNF+edi+u8Gq68GVmIdSSX9fvokx+9PGpJlNuznJB1lzB79OvV4zYSBEnBcavY9XJ83fsByv4KfVLAZNW2XKDcT1Kww=");

    assertTrue(isSignatureValid);

  }

  @Test
  void validateFailure() {

    final boolean isSignatureValid = signatureValidationService.validate("123456",
        "YsUAI+s8wcGwB3ZsJFfRpOu6tIzZweZXC4qUhj9f7qFcuuQuj8cE9ETM5D1MdEoSIAdGDuGY2QbhxVxtaNF+edi+u8Gq68GVmIdSSX9fvokx+9PGpJlNuznJB1lzB79OvV4zYSBEnBcavY9XJ83fsByv4KfVLAZNW2XKDcT1Kww="
            .replace('a', 'b'));

    assertFalse(isSignatureValid);

  }
}
