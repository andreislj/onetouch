package com.coingaming.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.coingaming.rest.model.Currency;
import com.coingaming.rest.model.DebitPartnerWalletRequest;
import com.coingaming.rest.model.PartnerResponseStatus;
import com.coingaming.rest.model.PartnerWalletResponse;
import com.coingaming.service.SignService;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class WalletRestControllerIntegrationTest {

  @Inject
  private ObjectMapper objectMapper;

  @Inject
  private SignService signService;

  @Inject
  private TestRestTemplate restTemplate;

  @Test
  void placeBetNoXSignatureHeader() {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    final HttpEntity<DebitPartnerWalletRequest> request = new HttpEntity<>(null, headers);
    final ResponseEntity<PartnerWalletResponse> responseEntity = restTemplate.exchange("/transaction/bet", HttpMethod.POST, request, PartnerWalletResponse.class);

    assertEquals(401, responseEntity.getStatusCodeValue());
    assertEquals(PartnerResponseStatus.RS_ERROR_INVALID_SIGNATURE, responseEntity.getBody().getStatus());

  }

  @Test
  void placeBetInvalidSignature() {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.add("X-Signature", "invalid signature");

    final HttpEntity<DebitPartnerWalletRequest> request = new HttpEntity<>(null, headers);
    final ResponseEntity<PartnerWalletResponse> responseEntity = restTemplate.exchange("/transaction/bet", HttpMethod.POST, request, PartnerWalletResponse.class);

    assertEquals(401, responseEntity.getStatusCodeValue());
    assertEquals(PartnerResponseStatus.RS_ERROR_INVALID_SIGNATURE, responseEntity.getBody().getStatus());

  }

  @Test
  void placeBet() throws Exception {

    final DebitPartnerWalletRequest body = new DebitPartnerWalletRequest("1", "2", "3", "4", 1, Currency.BTC, "5", "6", 2);
    final String content = objectMapper.writeValueAsString(body);
    final String signature = signService.sign(content);

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.add("X-Signature", signature);

    final HttpEntity<DebitPartnerWalletRequest> request = new HttpEntity<>(body, headers);
    final ResponseEntity<PartnerWalletResponse> responseEntity = restTemplate.exchange("/transaction/bet", HttpMethod.POST, request, PartnerWalletResponse.class);

    assertEquals(200, responseEntity.getStatusCodeValue());

//    final PartnerWalletResponse partnerWalletResponse = restTemplate.postForObject("/transaction/bet", request, PartnerWalletResponse.class);

  }

}
