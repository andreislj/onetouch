package com.coingaming.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.coingaming.OnetouchApiClient;
import com.coingaming.rest.model.ResponseUrl;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@WebMvcTest
@ContextConfiguration(classes = {GameRestController.class})
class GameRestControllerTest {

  @Inject
  private MockMvc mockMvc;

  @MockBean
  private OnetouchApiClient onetouchApiClient;

  @Test
  void getLandingUrl() throws Exception {

    Mockito.when(onetouchApiClient.makeCall("operator/generic/v2/game/url")).thenReturn(new ResponseUrl("https://game.url"));

    mockMvc
        .perform(get("/landing-url")
            .contentType(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
        )
        .andExpect(header().string("Location", "https://game.url"))
        .andExpect(status().isFound()).andReturn();
  }

}
