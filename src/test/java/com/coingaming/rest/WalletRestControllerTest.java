package com.coingaming.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.coingaming.rest.model.Currency;
import com.coingaming.rest.model.DebitPartnerWalletRequest;
import com.coingaming.rest.model.PartnerResponseStatus;
import com.coingaming.rest.model.PartnerWalletResponse;
import com.coingaming.service.PrivateKeyProvider;
import com.coingaming.service.SignService;
import com.coingaming.service.SignatureValidationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@WebMvcTest
@ContextConfiguration(classes = {WalletRestController.class})
class WalletRestControllerTest {

  @Inject
  private MockMvc mockMvc;

  @Inject
  private ObjectMapper objectMapper;

  @SpyBean
  private SignService signService;

  @SpyBean
  private PrivateKeyProvider privateKeyProvider;

  @MockBean
  private SignatureValidationService signatureValidationService;

  @Test
  void placeBetNoXSignatureHeader() throws Exception {

    final MvcResult result = mockMvc
        .perform(post("/transaction/bet")
            .contentType(APPLICATION_JSON)
        )
        .andExpect(
            status().isUnauthorized()
        ).andReturn();

    final PartnerWalletResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), PartnerWalletResponse.class);

    assertEquals(PartnerResponseStatus.RS_ERROR_INVALID_SIGNATURE, response.getStatus());

  }

  @Test
  void placeBetWrongSignature() throws Exception {

    final MvcResult result = mockMvc
        .perform(post("/transaction/bet")
            .header("X-Signature", "wrong signature")
            .contentType(APPLICATION_JSON)
        )
        .andExpect(
            status().isUnauthorized()
        ).andReturn();

    final PartnerWalletResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), PartnerWalletResponse.class);

    assertEquals(PartnerResponseStatus.RS_ERROR_INVALID_SIGNATURE, response.getStatus());

  }

  @Test
  void placeBet() throws Exception {

    final DebitPartnerWalletRequest request = new DebitPartnerWalletRequest("1", "2", "3", "4", 1, Currency.BTC, "5", "6", 2);
    final String content = objectMapper.writeValueAsString(request);
    final String signature = signService.sign(content);

    Mockito.when(signatureValidationService.validate(content, signature)).thenReturn(true);

    final MvcResult result = mockMvc
        .perform(
            post("/transaction/bet")
                .header("X-Signature", signature)
                .content(content)
                .accept(APPLICATION_JSON)
                .contentType(APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    final String responseContent = result.getResponse().getContentAsString();
    final PartnerWalletResponse response = objectMapper.readValue(responseContent, PartnerWalletResponse.class);

    assertEquals(PartnerResponseStatus.RS_OK, response.getStatus());

  }

}
