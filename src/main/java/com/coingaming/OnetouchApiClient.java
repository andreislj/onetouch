package com.coingaming;

import com.coingaming.rest.model.RequestUrl;
import com.coingaming.rest.model.ResponseUrl;
import com.coingaming.service.SignService;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class OnetouchApiClient {

  @Value("${onetouch.api.baseUrl}")
  private String onetouchApiBaseUrl;

  @Inject
  private ObjectMapper objectMapper;
  @Inject
  private SignService signService;
  @Inject
  private RestTemplate restTemplate;

  //TODO (29.07.2021) Andrei Sljusar: exception handling
  public ResponseUrl makeCall(final String uri) throws Exception {

    final RequestUrl body = new RequestUrl("3nYTOSjdlF6UTz9Ir", "XX", "EUR", 10, "cd6bd8560f3bb8f84325152101adeb45", "GPL_DESKTOP", 70000, "en", "https://examplecasino.io", "::ffff:10.0.0.39");
    final String s = objectMapper.writeValueAsString(body);
    final String signature = signService.sign(s);

    final String url = onetouchApiBaseUrl + uri;

    final MultiValueMap<String, String> headers = new HttpHeaders();
    headers.add("X-Signature", signature);
    final HttpEntity<RequestUrl> request = new HttpEntity<>(body, headers);
    //TODO (29.07.2021) Andrei Sljusar: error handling
    //TODO (29.07.2021) Andrei Sljusar: org.springframework.web.client.HttpClientErrorException$NotFound: 404 : [{"timestamp":"2021-07-29T07:33:52Z","status":404,"error":"Not Found","message":"No message available","path":"/operator1/generic/v2/game/url"}]
    //TODO (29.07.2021) Andrei Sljusar: org.springframework.web.client.HttpClientErrorException$Unauthorized: 401 : [no body]
    final ResponseEntity<ResponseUrl> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, ResponseUrl.class);
    return responseEntity.getBody();
  }

}
