package com.coingaming.service;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.util.Base64;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class SignService {

  @Inject
  private PrivateKeyProvider privateKeyProvider;

  // Create base64 encoded signature using SHA256/RSA.
  public String signSHA256RSA(final String data, final PrivateKey privateKey) throws Exception {
    final Signature privateSignature = Signature.getInstance("SHA256withRSA");
    privateSignature.initSign(privateKey);
    privateSignature.update(data.getBytes(StandardCharsets.UTF_8));
    final byte[] s = privateSignature.sign();
    return Base64.getEncoder().encodeToString(s);
  }

  public String sign(final String input) {
    final RSAPrivateKey rsaPrivateKey = privateKeyProvider.readPrivateKey();
    try {
      return signSHA256RSA(input, rsaPrivateKey);
    } catch (final Exception e) {
      throw new RuntimeException("Can't sign.", e);
    }
  }
}
