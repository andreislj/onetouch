package com.coingaming.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class SignatureValidationService {

  @Inject
  private PublicKeyProvider publicKeyProvider;

  public boolean validate(final String data, final String signatureAsString) {

    try {
      final RSAPublicKey publicKey = publicKeyProvider.readPublicKey();
      final byte[] receivedSignature = Base64.getDecoder().decode(signatureAsString.getBytes());
      final Signature signature = Signature.getInstance("SHA256withRSA");
      signature.initVerify(publicKey);
      signature.update(data.getBytes());
      final boolean verifies = signature.verify(receivedSignature);
      return verifies;
    } catch (final NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
      throw new RuntimeException(e);
    }

  }

}
