package com.coingaming.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class PublicKeyProvider {


  //TODO (03.08.2021) Andrei Sljusar: cache
  public RSAPublicKey readPublicKey() {
    final ClassPathResource classPathResource = new ClassPathResource("public_test_task.pem");
    try {
      return readPublicKey(classPathResource.getFile());
    } catch (final IOException e) {
      throw new RuntimeException("Can't rad public key", e);
    }
  }

  private RSAPublicKey readPublicKey(final File file) {

    try {
      final String key = Files.readString(file.toPath(), Charset.defaultCharset());
      final String publicKeyPEM = key
          .replace("-----BEGIN PUBLIC KEY-----", "")
          .replaceAll(System.lineSeparator(), "")
          .replace("-----END PUBLIC KEY-----", "");
      final byte[] encoded = java.util.Base64.getDecoder().decode(publicKeyPEM);
      final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
      return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    } catch (final IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
      throw new RuntimeException(e);
    }
  }

}
