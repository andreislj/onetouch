package com.coingaming.service;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class PrivateKeyProvider {

  public RSAPrivateKey readPrivateKey() {
    final ClassPathResource classPathResource = new ClassPathResource("myrsakey_pcks8");
    try {
      return readPrivateKey(classPathResource.getFile());
    } catch (final Exception e) {
      throw new RuntimeException("Can't read private key.");
    }
  }

  private RSAPrivateKey readPrivateKey(final File file) throws Exception {
    final String key = Files.readString(file.toPath(), Charset.defaultCharset());

    final String privateKeyPEM = key
        .replace("-----BEGIN PRIVATE KEY-----", "")
        .replaceAll(System.lineSeparator(), "")
        .replace("-----END PRIVATE KEY-----", "");

    final byte[] encoded = java.util.Base64.getDecoder().decode(privateKeyPEM);

    final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    final PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
    return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
  }

}
