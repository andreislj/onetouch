package com.coingaming;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig implements WebMvcConfigurer {

  /*@Inject
  private XSignatureHeaderInterceptor xSignatureHeaderInterceptor;

  @Override
  public void addInterceptors(final InterceptorRegistry registry) {
    registry.addInterceptor(xSignatureHeaderInterceptor).addPathPatterns("/transaction/**");
  }*/

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @Bean
  public FilterRegistrationBean<CommonsRequestLoggingFilter> loggingFilter() {

    final FilterRegistrationBean<CommonsRequestLoggingFilter> registrationBean = new FilterRegistrationBean<>();
    final CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();

    filter.setIncludePayload(true);
    filter.setIncludeHeaders(true);
    registrationBean.setFilter(filter);
    registrationBean.addUrlPatterns("/transaction/*");

    return registrationBean;
  }

}
