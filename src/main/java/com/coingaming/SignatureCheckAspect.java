package com.coingaming;


import com.coingaming.rest.model.DebitPartnerWalletRequest;
import com.coingaming.rest.model.PartnerResponseStatus;
import com.coingaming.rest.model.PartnerWalletResponse;
import com.coingaming.service.SignatureValidationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

//@Aspect
//@Component
public class SignatureCheckAspect {

  @Inject
  private SignatureValidationService signatureValidationService;
  @Inject
  private ObjectMapper objectMapper;

  //TODO (03.08.2021) Andrei Sljusar: make it generic
  @Around("@annotation(SignatureCheck) && args(request, xSignature)")
  public Object checkSignature(ProceedingJoinPoint pjp, DebitPartnerWalletRequest request, String xSignature) throws Throwable {
    if (xSignature == null) {
      return new ResponseEntity<>(new PartnerWalletResponse(PartnerResponseStatus.RS_ERROR_INVALID_SIGNATURE), HttpStatus.UNAUTHORIZED);
    }
    final String content = objectMapper.writeValueAsString(request);
    final boolean isSignatureValid = isSignatureValid(xSignature, content);
    if (isSignatureValid) {
      return pjp.proceed();
    }

    return new ResponseEntity<>(new PartnerWalletResponse(PartnerResponseStatus.RS_ERROR_INVALID_SIGNATURE), HttpStatus.UNAUTHORIZED);

  }

  private boolean isSignatureValid(final String xSignature, final String content) {
    try {
      return signatureValidationService.validate(content, xSignature);
    } catch (final Exception e) {
      e.printStackTrace();
      return false;
    }
  }
}
