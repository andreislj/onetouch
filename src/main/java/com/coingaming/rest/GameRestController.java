package com.coingaming.rest;

import com.coingaming.OnetouchApiClient;
import com.coingaming.rest.model.ResponseUrl;
import javax.inject.Inject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//**Games API** is an API that returns the game URL and the list of available games. Implemented by the OneTouch.
//All **Games API** requests have to be signed by Operator.
@RestController
class GameRestController {

  @Inject
  private OnetouchApiClient onetouchApiClient;

  //Obtain a game URL from OneTouch. Direct the customer to the URL provided by OneTouch.

  //1) endpoint which will request our API (/operator/generic/v2/game/url) and after receiving the url, will redirect to this page.
  //Returns the Landing URL of the chosen game that can be embedded into the website:
  @GetMapping("/landing-url")
  ResponseEntity<String> getLandingUrl() throws Exception {
    final ResponseUrl response = onetouchApiClient.makeCall("operator/generic/v2/game/url");
    final HttpHeaders headers = new HttpHeaders();
    headers.add("Location", response.getUrl());
    return new ResponseEntity<>(headers, HttpStatus.FOUND);
  }

}
