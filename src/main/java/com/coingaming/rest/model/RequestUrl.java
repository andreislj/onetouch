package com.coingaming.rest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RequestUrl {

  private String user;
  private String country;
  private String currency;
  private Integer operator_id;
  private String token;
  private String platform;
  private Integer game_id;
  private String lang;
  private String lobby_url;
  private String ip;

}
