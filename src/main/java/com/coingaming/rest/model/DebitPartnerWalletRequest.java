package com.coingaming.rest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DebitPartnerWalletRequest {

  private String transaction_uuid;
  private String token;
  private String round;
  private String request_uuid;
  private Integer game_id;
  private Currency currency;
  private String offer_id;
  private String bet;

  /*amount*	money_amountinteger
example: 100500
Integer is used to represent the amount of money. Precision depends on the currency. Example: $3.56 must be represented as 356000.
Following enum shows precision for different currencies. Format: currency:precision

Enum:
[ ANG = 5, SOS = 5, PLN = 5, VND =*/

  private Integer amount;

}
