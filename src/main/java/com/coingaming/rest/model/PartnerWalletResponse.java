package com.coingaming.rest.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PartnerWalletResponse {

  private String user;
  private PartnerResponseStatus status;
  private String request_uuid;
  private Currency currency;
  private Integer balance;

  public PartnerWalletResponse(final PartnerResponseStatus status) {
    this.status = status;
  }
}
