package com.coingaming.rest;

import com.coingaming.rest.model.Currency;
import com.coingaming.rest.model.DebitPartnerWalletRequest;
import com.coingaming.rest.model.PartnerResponseStatus;
import com.coingaming.rest.model.PartnerWalletResponse;
import com.coingaming.service.SignatureValidationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

//**Wallet API** is An API that OneTouch triggers to manipulate the user's balance. Implemented by the Operator.
@RestController
class WalletRestController {

  @Inject
  private SignatureValidationService signatureValidationService;

  @Inject
  private ObjectMapper objectMapper;

  //https://app.swaggerhub.com/apis/onetouch/onetouch/2.0#/Wallet%20API/%2Ftransaction%2Fbet
//  @SignatureCheck
  @PostMapping(value = "/transaction/bet", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  ResponseEntity<PartnerWalletResponse> placeBet(@RequestBody(required = false) final DebitPartnerWalletRequest request, @RequestHeader(name = "X-Signature", required = false) final String xSignature)
      throws Exception {

    if (xSignature == null) {
      return new ResponseEntity<>(new PartnerWalletResponse(PartnerResponseStatus.RS_ERROR_INVALID_SIGNATURE), HttpStatus.UNAUTHORIZED);
    }

    final String content = objectMapper.writeValueAsString(request);
    final boolean isSignatureValid = isSignatureValid(xSignature, content);

    if (isSignatureValid) {
      return new ResponseEntity<>(new PartnerWalletResponse("Jimm123", PartnerResponseStatus.RS_OK, "16d2dcfe-b89e-11e7-854a-58404eea6d16", Currency.USD, 100500), HttpStatus.OK);
    }

    return new ResponseEntity<>(new PartnerWalletResponse(PartnerResponseStatus.RS_ERROR_INVALID_SIGNATURE), HttpStatus.UNAUTHORIZED);
  }

  private boolean isSignatureValid(final String xSignature, final String content) {
    try {
      return signatureValidationService.validate(content, xSignature);
    } catch (final Exception e) {
      e.printStackTrace();
      return false;
    }
  }

}
